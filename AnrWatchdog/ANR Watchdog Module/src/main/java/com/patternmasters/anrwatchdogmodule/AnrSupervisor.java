package com.patternmasters.anrwatchdogmodule;

import android.content.pm.PackageManager;
import android.os.Looper;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.unity3d.player.UnityPlayer;

/**
 * A class supervising the UI thread for ANR errors. Use
 * {@link #start()} and {@link #stop()} to control
 * when the UI thread is supervised
 */
public class AnrSupervisor {
    private static String mLogPath;
    private AnrSupervisorRunnable mSupervisor;

    /**
     * The {@link ExecutorService} checking the UI thread
     */
    private ExecutorService mExecutor =
            Executors.newSingleThreadExecutor();
    /**
     * The {@link AnrSupervisorRunnable} running on a separate
     * thread
     */


    /**
     * Starts the supervision
     */
    public synchronized void start() {
        mSupervisor = new AnrSupervisorRunnable(Looper.myLooper(), mLogPath);
        synchronized (this.mSupervisor) {
            if (this.mSupervisor.isStopped()) {
                this.mExecutor.execute(this.mSupervisor);

            } else {
                this.mSupervisor.unstopp();

            }
        }
    }

    public synchronized  void setLogPath(String logPath) {
        mLogPath = logPath;
    }

    /**
     * Stops the supervision. The stop is delayed, so if
     * start() is called right after stop(),
     * both methods will have no effect. There will be at least one
     * more ANR check before the supervision is stopped.
     */
    public synchronized void stop() {
        this.mSupervisor.stop();

    }
}
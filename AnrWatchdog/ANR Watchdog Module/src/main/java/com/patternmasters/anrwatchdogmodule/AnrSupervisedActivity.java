package com.patternmasters.anrwatchdogmodule;

import androidx.appcompat.app.AppCompatActivity;

public class AnrSupervisedActivity extends AppCompatActivity {
    static final AnrSupervisor sSupervisor = new AnrSupervisor();
    public void onStart() {
        super.onStart();
        sSupervisor.start();

    }
    public void onStop() {
        super.onStop();
        sSupervisor.stop();
    }
}

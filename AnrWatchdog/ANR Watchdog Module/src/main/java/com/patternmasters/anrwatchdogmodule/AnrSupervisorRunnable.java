package com.patternmasters.anrwatchdogmodule;
import android.os.*;
import android.util.Log;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * A {@link Runnable} testing the UI thread every 10s until {@link
 * #stop()} is called
 */
public class AnrSupervisorRunnable implements Runnable {

    private final SimpleDateFormat mDateFormatter = new SimpleDateFormat("HH:mm:ss.SSS");

    /**
     * The {@link Handler} to access the UI threads message queue
     */
    private Handler mHandler; //= new Handler(Looper.getMainLooper());

    /**
     * The stop flag
     */
    private boolean mStopped;

    /**
     * Flag indicating the stop was performed
     */
    private boolean mStopCompleted = true;


    /**
     * PrintWriter that writes logs to a file
     */
    private PrintWriter mLogWriter;


    public AnrSupervisorRunnable(Looper looper, String logPath) {
        mHandler = new Handler(looper);
        if (logPath != null)
           createLogFile(logPath);
    }

    @Override
    public void run() {
        this.mStopCompleted = false;

        // Loop until stop() was called or thread is interrupted
        while (!Thread.interrupted()) {
            try {
                // Log
                //Log.d(AnrSupervisor.class.getSimpleName(),
                //        "Check for ANR...");

                // Create new callback
                AnrSupervisorCallback callback =
                        new AnrSupervisorCallback();

                // Perform test, Handler should run
                // the callback within 4s
                synchronized (callback) {
                    this.mHandler.post(callback);
                    callback.wait(4000);

                    // Check if called
                    if (!callback.isCalled()) {
                        writeLog("UI Thread failed to respond within 4s");

                        // Wait until the thread responds again
                        callback.wait();
                        writeLog("Connection to UI Thread was restored\n");
                    } else {
                        //Log.d(AnrSupervisor.class.getSimpleName(),
                        //        "UI Thread responded within 4s");
                    }
                }

                // Check if stopped
                this.checkStopped();

                // Sleep for next test
                Thread.sleep(1000);

            } catch (InterruptedException e) {
                break;
            }
        }

        // Set stop completed flag
        this.mStopCompleted = true;

        // Log
        //Log.d(AnrSupervisor.class.getSimpleName(),
        //        "ANR supervision stopped");

    }

    private synchronized void createLogFile(String logPath) {
        try {
            System.out.println("Initiating ANRWatchdog logs at : " + logPath);

            File logFile = new File(logPath);
            logFile.getParentFile().mkdirs();
            if (!logFile.exists())
            {
                try
                {
                    logFile.createNewFile();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }

            FileWriter fileWriter = new FileWriter(logPath, false);
            mLogWriter = new PrintWriter(fileWriter);

            mDateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            if (mLogWriter == null)
                throw new NullPointerException("PrintWriter for log file is null");

        } catch (Exception e) {
            System.out.println("An error occurred while initiating ANRWatchdog log file");
            e.printStackTrace();
        }
    }

    private synchronized  void writeLog(String log) {
        Log.d(AnrSupervisor.class.getSimpleName(), log);

        if (mLogWriter == null)
            return;

        String timestamp = mDateFormatter.format(new Date(System.currentTimeMillis()));
        mLogWriter.printf("%s: %s\n", timestamp, log);
        mLogWriter.flush();
    }
    private synchronized void checkStopped()
            throws InterruptedException {
        if (this.mStopped) {
            // Wait 1000ms
            Thread.sleep(1000);

            // Break if still stopped
            if (this.mStopped) {
                throw new InterruptedException();

            }
        }
    }

    /**
     * Stops the check
     */
    synchronized void stop() {
        Log.d(AnrSupervisor.class.getSimpleName(), "Stopping...");
        this.mStopped = true;

    }

    /**
     * Stops the check
     */
    synchronized void unstopp() {
        Log.d(AnrSupervisor.class.getSimpleName(),
                "Revert stopping...");
        this.mStopped = false;

    }

    /**
     * Returns whether the stop is completed
     *
     * @return true if stop is completed, false if not
     */
    synchronized boolean isStopped() {
        return this.mStopCompleted;

    }
}
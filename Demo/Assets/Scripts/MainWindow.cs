using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

namespace Demo
{
    public class MainWindow : MonoBehaviour
    {
        [SerializeField]
        private Button _whileTrueBtn;

        [SerializeField]
        private Button _whileTrue5SecBtn;

        [SerializeField]
        private Button _whileTrue10SecBtn;

        [SerializeField]
        private Button _desirializeJson200Mb;

        [SerializeField]
        private Button _threadSleep;

        private void Awake()
        {
            _whileTrueBtn.onClick.AddListener(WhileTrue);
            _whileTrue5SecBtn.onClick.AddListener(() => StartCoroutine(WhileTrueCoroutine(5)));
            _whileTrue10SecBtn.onClick.AddListener(() => StartCoroutine(WhileTrueCoroutine(10)));
            _desirializeJson200Mb.onClick.AddListener(DeserializeJson);
            _threadSleep.onClick.AddListener(ThreadSleep);

        }

        private void WhileTrue()
        {
            while (true)
            { }
        }

        private void ThreadSleep()
        {
            Thread.Sleep(1000*30);
        }

        private async Task WhileTrueAsync(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {}
            Debug.Log("while(true) finished");
        }

        private IEnumerator WhileTrueCoroutine(float time)
        {
            var source = new CancellationTokenSource();
            var token = source.Token;
            Task.Run(() => WhileTrueAsync(token));

            yield return new WaitForSeconds(time);

            source.Cancel();
        }

        private void DeserializeJson()
        {
            var deserializer = new JsonDeserialize();
            deserializer.DeserializeAsset();
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace Demo
{
    public class JsonDeserialize
    {
        private class CityLots
        {
            public string Type;
            public Feature[] Features;
        }

        private class Feature
        {
            public string Type;
            public Dictionary<string, string> Properties;
        }
        private TextAsset _asset;

        public JsonDeserialize()
        {
            _asset = Resources.Load<TextAsset>("citylots");
        }

        public void DeserializeAsset()
        {
            var result = JsonConvert.DeserializeObject<CityLots>(_asset.text);
            if (result != null)
                Debug.Log("Deserialization succeeded");
            else
                Debug.Log("Deserialization failed");
        }
    }

}

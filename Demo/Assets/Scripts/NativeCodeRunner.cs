using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
public class NativeCodeRunner : MonoBehaviour
{
    void Start()
    {
        CallNativePlugin();
    }
    //method that calls our native plugin.
    public void CallNativePlugin()
    {
        //AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        //AndroidJavaObject unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject supervisor = new AndroidJavaObject("com.patternmasters.anrwatchdogmodule.AnrSupervisor");

        supervisor.Call("setLogPath", Path.Combine(Application.persistentDataPath, "ANRLogs.txt"));
        supervisor.Call("start");
    }
}